<%-- 
    Document   : Testing
    Created on : 17-oct-2016, 20:45:01
    Author     : WNRO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--<link  type="image/x-icon" href="../../static/images/iconos/escudo.ico" rel="shortcut icon"/>-->
<!--        <link type="text/css"  href="../../static/css/webstyle.css">
        <link  type="text/css" href="../../static/css/jquery-ui-1.8.23.custom.css">
        <link  type="text/css" href="../../static/css/ui.jqgrid.css">
        <link  type="text/css" href="../../static/css/menu_style.css">-->
        
        <link rel="stylesheet" type="text/css" href="../../static/css/webstyle.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/jquery-ui-1.8.23.custom.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/ui.jqgrid.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/menu_style.css">



        <script type="text/javascript" src="../../static/js/config/jquery.js"></script>
        <script  type="text/javascript" src="../../static/js/config/jqueryui.js"></script>
        <script  type="text/javascript" src="../../static/js/config/jquery.maskedinput-1.3.js"></script>
        <script type="text/javascript"   src="../../static/js/config/jstepper.js"></script>
        <script  type="text/javascript"   src="../../static/js/config/jquery.jqGrid.min.js"></script>
        <script  type="text/javascript" src="../../static/js/config/grid.locale-es.js"></script>
        <script>
            $(document).ready(function () {
                $.getScript("../../static/js/general/Testing.js");
            });
        </script>
        <style>
            .ui-jqgrid tr.jqgrow td {
                white-space: normal !important;
            }
        </style>
    </head>
    <body>

        <input id="txtPretensionSeleccionada" type="hidden" value=""/>
        <div class="wrapper">
            <div class="shadowcontainer">
                <div>
                    <div class="maincontainer" >
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Top.jspf"/>--%>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Menu.jspf"/>--%>
                        </div>
                        <div>
                            <div class="workareacontainer ui-widget-content">
                                <div class="container" >
                                    <div style="text-align: center" ><h1>ENCUESTA DE CLIMA ORGANIZACIONAL</h1></div>
                                </div>
                                <div class="container">


                                    <!-- Opcion de informacion -->
                                    <div id="divMensajeSistema" style="display:none" class="ui-widget">
                                        <div id="divNoticiaSistema" class="">
                                            <table>
                                                <tr>
                                                    <td> <span id="iconoNoticiaSistema" style=" margin-right: .3em;" class=""></span></td>
                                                    <td> <span id="textoNoticiaSistema" ></span></td>
                                                    <td style="text-align:center; margin-left:55%; border-left-style:solid">Cerrar Mensaje</td>
                                                    <td style="text-align:center"> <span id="cerrarNoticiaSistema" ></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- fin de opcion de información.-->
                                </div>



                                <div class="container" id="divDatosFormulario">

                                    <fieldset>
                                        <legend><b>Datos generales de identificación</b></legend>
                                        <table>
                                            <tr>
                                                <td>Genero: 
                                                    <select id="slGenero" name="slGenero">
                                                        <option value="00">-SELECCIONE-</option>
                                                        <option value="0">FEMENINO</option>
                                                        <option value="1">MASCULINO</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    Procuraduria:
                                                    <select id="slProcuradurias" name="slProcuradurias">

                                                    </select>
                                                </td>
                                                <td>
                                                    Unidad:
                                                    <select id="slUnidades" name="slUnidades">

                                                    </select>
                                                </td>
                                            </tr>
                                        </table>

                                    </fieldset>
                                    <fieldset>
                                        <legend><b>Encuesta de Clima</b></legend>
                                        <table id="TdPreguntasClima">
                                            <tr>
                                                <td>1</td>
                                                <td>¿Conozco la Visión y Misión Institucional?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta1" name="txtPregunta1" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>¿Dispongo de los recursos necesarios para realizar mi trabajo?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta2" name="txtPregunta2" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>¿El trabajo para mí es realmente interesante?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta3" name="txtPregunta3" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>¿Existe el trabajo en equipo en el área en que me desempeño?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta4" name="txtPregunta4" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>¿Mi superior inmediato demuestra tener la competencia necesaria para el desempeño de su cargo?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta5" name="txtPregunta5" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>¿Considero que mi superior inmediato es receptivo a sugerencias?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta6" name="txtPregunta6" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>¿Considero que mi superior inmediato valora positivamente mi trabajo?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta7" name="txtPregunta7" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>¿Considero que mi superior inmediato se preocupa más por aprovechar el talento humano para lograr buenos resultados, que hacerlo a un lado por miedo a perder el poder? </td>
                                                <td>
                                                    <input type="text" id="txtPregunta8" name="txtPregunta8" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>¿Considero que mi salario está de acuerdo a mis responsabilidades que asumo, a los conocimientos y competencias que desarrollo en mi trabajo? </td>
                                                <td>
                                                    <input type="text" id="txtPregunta9" name="txtPregunta9" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>¿Encuentro a mis compañeros/as de trabajo motivados/as y satisfechos/as con el trabajo que realizan?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta10" name="txtPregunta10" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>11</td>
                                                <td>¿Considero que existe una carga de trabajo de forma equitativa y acorde a la competencia y desempeño de mis compañeros/as de trabajo?  </td>
                                                <td>
                                                    <input type="text" id="txtPregunta11" name="txtPregunta11" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>12</td>
                                                <td>¿Existe un adecuado nivel de participación del personal en la toma de decisiones  de la institución?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta12" name="txtPregunta12" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>13</td>
                                                <td>¿Mis sugerencias y propuestas son tomadas en cuenta por mis superiores?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta13" name="txtPregunta13" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>14</td>
                                                <td>¿Tengo posibilidades de promoción y ascenso en la institución?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta14" name="txtPregunta14" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>15</td>
                                                <td>¿Existe igualdad de oportunidades de desarrollo humano y técnico para el personal?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta15" name="txtPregunta15" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>16</td>
                                                <td>¿Mi superior inmediato controla al personal y lo supervisa frecuentemente?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta16" name="txtPregunta16" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>17</td>
                                                <td>¿A menudo la gente habla de los compañeros a sus espaldas, propiciando la comunicación informal, creando un clima laboral desfavorable?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta17" name="txtPregunta17" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>18</td>
                                                <td>¿Mi superior inmediato se preocupa por mantener un buen clima laboral, para lograr mejores resultados?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta18" name="txtPregunta18" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>19</td>
                                                <td>¿Considero que muchos de los compañeros/as de trabajo, están pendientes del reloj para dejar el trabajo?  </td>
                                                <td>
                                                    <input type="text" id="txtPregunta19" name="txtPregunta19" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>20</td>
                                                <td>¿Considero que las instalaciones y espacios físicos, reúnen las condiciones de seguridad e higiene para realizar  mi trabajo?</td>
                                                <td>
                                                    <input type="text" id="txtPregunta20" name="txtPregunta20" size="4" maxlength="1"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                </td>
                                            </tr>

                                        </table>
                                    </fieldset>
                                    <fieldset>
                                        <legend><b>Comentariosa</b></legend>
                                        <textarea placeholder="Comentarios" id="txtaObservacionesClima" name="txtaObservacionesClima" rows="4" cols="80">.</textarea>
                                        <p></p>
                                        <button id="cmdRegistrarClima">Registrar</button>
                                        <button id="cmdCancelarClima">Cancelar</button>
                                    </fieldset>
                                    <fieldset>
                                        <legend><b>Control Registros</b></legend>
                                        <table id="tbControlClima" width="100%">
                                            <tr><td></td></tr>
                                        </table>
                                        <div id="barraControlClima"></div>
                                    </fieldset>

                                </div>


                                <div class="container">
                                    <!-- Opcion de informacion -->
                                    <div id="divInformativo" style="display:none" class="ui-widget">
                                        <div id="divNoticia" class="">
                                            <table>
                                                <tr>
                                                    <td> <span id="iconoNoticia" style=" margin-right: .3em;" class=""></span></td>
                                                    <td> <span id="textoNoticia" ></span></td>
                                                    <td style="text-align:center; margin-left:55%">. Cerrar Mensaje</td>
                                                    <td style="text-align:center"> <span id="cerrarNoticia" ></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- fin de opcion de información.-->
                                </div>
                            </div>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Footer.jspf"/>--%>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
