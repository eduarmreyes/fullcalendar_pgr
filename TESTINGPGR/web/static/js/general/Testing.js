/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#txtaObservacionesClima").val("");
$("#TdPreguntasClima :input").jStepper({minValue: 0, maxValue: 3, minLength: 1});
$("#cmdRegistrarClima,#cmdCancelarClima").button();


$.ajax({
    type: "POST",
    url: "/SERH/PGR/Encuesta?opcion=unidadesOrganizacionales",
    dataType: 'json',
    success: function (data) {
        $("#slUnidades").append("<option value='00'>-SELECCIONE-</option>");
        $.each(data.rows, function (i, rows) {
            codigo = "<option value='" + rows.cell[0] + "'>" + rows.cell[1] + "</option>";
            $("#slUnidades").append(codigo);

        });
    },
    error: function () {
        errorServidor();
    }
});

$.ajax({
    type: "POST",
    url: "/SERH/PGR/Encuesta?opcion=procuraduriaGenerales",
    dataType: 'json',
    success: function (data) {
        $("#slProcuradurias").append("<option value='00'>-SELECCIONE-</option>");
        $.each(data.rows, function (i, rows) {
            codigo = "<option value='" + rows.cell[0] + "'>" + rows.cell[1] + "</option>";
            $("#slProcuradurias").append(codigo);

        });
    },
    error: function () {
        errorServidor();
    }
});

function errorServidor() {
    Alert("Hubo un error al ejecutar la consulta, al parecer la sesion se ha finalizado. Cierre el sistema SERH y vuelva a ingresar.");
}

$("#cmdRegistrarClima").click(function (e) {
    e.preventDefault();
    var accesoCorrecto = validarDatos("TdPreguntasClima");
    if (accesoCorrecto && $("#slGenero").val()!="00" && $("#slProcuradurias").val()!="00") {
        var registrosDatos = $("#TdPreguntasClima :input").serialize();
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/Encuesta?opcion=registrarClima&" + registrosDatos + "&slGenero=" + $("#slGenero").val() + "&slProcuradurias=" + $("#slProcuradurias").val() + "&slUnidades=" + $("#slUnidades").val() + "&txtaObservacionesClima=" + $("#txtaObservacionesClima").val(),
            dataType: 'json',
            success: function (data) {
                if (data.accion == "1") {
                    alert("Se registro con exito");
                    $("#TdPreguntasClima :input").val("");
                    $("#slProcuradurias").val("00");
                    $("#slUnidades").val("00");
                    $("#slGenero").val("00");
                    $("#txtaObservacionesClima").val("");
                    recargarGridConteoClima();
                }
                else
                    alert("ups! Hubo un error interno del sistema. Contacte al Administrador");
            },
            error: function () {
                alert("al parecer se ha finalizado la sesion. Presiona la tecla F5 o Recargue la Pagina del sistema");
            }
        });
    } else {
        alert("Verifique que los datos del Sexo, Procuraduria hayan sido selecciodados y que no existan ninguna pregunta sin valor de respuesta.")
    }

});

$("#tbControlClima").jqGrid({
    url: '/SERH/PGR/Encuesta?opcion=controlClima',
    datatype: "json",
    colNames: ['Procuraduria', 'Unidad', 'Sexo', 'cantidad'],
    colModel: [
        {
            name: 'Procuraduria',
            index: 'Procuraduria',
            hidden: true,
            width: 20
        },
        {
            name: 'Unidad',
            index: 'Unidad',
            hidden: false ,
            width: 15
        },
        {
            name: 'Sexo',
            index: 'Sexo',
            hidden: false,
            width: 5
        },
        {
            name: 'cantidad',
            index: 'cantidad',
            width: 5
        }
    ],
    rowNum: 1000,
    rowList: [1000, 100, 10],
    pager: '#barraControlClima',
    sortname: 'id',
    height: 'auto',
    //height: "100%",//ajustar tamaño a la cantidad de filas
    autowidth: true,
    viewrecords: true,
    ondblClickRow: function(rowid) {
        //jQuery(this).jqGrid('editGridRow', rowid);
        //generarImpresion();
    },
    grouping: true,
    groupingView: {
        groupField: ['Procuraduria'],
        groupColumnShow: [true],
        groupText: ['<b>{0} - {1} Empleado(s)</b>'],
        groupCollapse: true,
        groupOrder: ['asc'],
        groupDataSorted: true
    },
    sortorder: "desc",
    caption: "Lista de Encuesta de Clima de la PGR"
});
jQuery("#tbControlClima").jqGrid('navGrid', '#barraControlClima', {
    edit: false,
    add: false,
    del: false
});
function recargarGridConteoClima() {
    jQuery("#tbControlClima").jqGrid().setGridParam({
        url: '/SERH/PGR/Encuesta?opcion=controlClima',
    }).trigger("reloadGrid");

}

function validarDatos(elemento) {
    var valido = true;
    $("#" + elemento + " :input[type=text]").each(function (i) {
        var campo = $(this).val();
        if (campo == "" || campo.length == 0 || campo < 0 || campo > 5) {
            $(this).addClass("warning");
            valido = false;
        }

    });
    $("input[type=text]").focusin(function () {
        $(this).removeClass("warningText");
        $(this).removeClass("warning");
    })
    $("input[type=text]").click(function () {
        $(this).removeClass("warningText");
        $(this).removeClass("warning");
    });
    return valido;

}
$("#cerrarNoticia").click(function () {
    removerMensajeAdvertencia();
    removerMensajeError();
});
function removerMensajeAdvertencia() {
    $("#divNoticia").removeClass("ui-state-highlight ui-corner-all");
    $("#iconoNoticia").removeClass("ui-icon ui-icon-info ui-icon-alert");
    $("#cerrarNoticia").removeClass("ui-icon ui-icon-circle-close");
    $("#textoNoticia").html("")
    $("#divInformativo").hide();
}