/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$("#tbPuestos").jqGrid({
    url: '/SERH/PGR/Puestos?opcion=listadoPuestos',
    datatype: "json",
    colNames: ['Procuraduria', 'Unidad', 'Sexo', 'cantidad'],
    colModel: [
        {
            name: 'Procuraduria',
            index: 'Procuraduria',
            hidden: true,
            width: 20
        },
        {
            name: 'Unidad',
            index: 'Unidad',
            hidden: false ,
            width: 15
        },
        {
            name: 'Sexo',
            index: 'Sexo',
            hidden: false,
            width: 5
        },
        {
            name: 'cantidad',
            index: 'cantidad',
            width: 5
        }
    ],
    rowNum: 1000,
    rowList: [1000, 100, 10],
    pager: '#barraPuestos',
    sortname: 'id',
    height: 'auto',
    //height: "100%",//ajustar tamaño a la cantidad de filas
    autowidth: true,
    viewrecords: true,
    ondblClickRow: function(rowid) {
        //jQuery(this).jqGrid('editGridRow', rowid);
        //generarImpresion();
    },
    grouping: true,
    groupingView: {
        groupField: ['Procuraduria'],
        groupColumnShow: [true],
        groupText: ['<b>{0} - {1} Empleado(s)</b>'],
        groupCollapse: true,
        groupOrder: ['asc'],
        groupDataSorted: true
    },
    sortorder: "desc",
    caption: "Lista de Puestos"
});
jQuery("#tbPuestos").jqGrid('navGrid', '#barraPuestos', {
    edit: false,
    add: false,
    del: false
});