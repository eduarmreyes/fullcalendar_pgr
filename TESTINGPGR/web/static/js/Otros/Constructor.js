/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#txtNuevaHabilidad,#txtNuevoComportamiento").val("");
$("#divDatosFormulario,#divConstructorHabilidadesCompetencias,#tdNuevoComportamiento,#tdNuevaHabilidad").hide();
$("button").button();
$("#cmdAgregarHabilidad,#cmdAgregarComportamiento").button({
    icons: {
        primary: "ui-icon ui-icon-circle-arrow-w"//,
                //secondary: "ui-icon-triangle-1-s"
    },
    text: false
});
$("#cmdEliminarHabilidad,#cmdEliminaComportamiento").button({
    icons: {
        primary: "ui-icon ui-icon-closethick"//,
                //secondary: "ui-icon-triangle-1-s"
    },
    text: false
});

$("#cmdNuevaHabilidad,#cmdNuevoComportamiento").button({
    icons: {
        primary: "ui-icon ui-icon-plus"//,
                //secondary: "ui-icon-triangle-1-s"
    },
    text: false
});


$("#tbFormularios").jqGrid({
    url: '/SERH/PGR/Constructor?opcion=listaFormularios',
    datatype: "json",
    colNames: ['IdFormulario', 'Codigo', 'Nombre del Formulario', 'Descripción', 'Fecha Creacion', 'Activo/Inactivo'],
    colModel: [
        {
            name: 'IdFormulario',
            index: 'IdFormulario',
            hidden: true,
            align: "left",
            width: 55
        }, {
            name: 'CodigoFormulario',
            index: 'CodigoFormulario',
            hidden: false,
            align: "center",
            width: 55
        },
        {
            name: 'NombreFormulario',
            index: 'NombreFormulario',
            hidden: false,
            width: 250
        },
        {
            name: 'DescripcionFormulario',
            index: 'DescripcionFormulario',
            hidden: false,
            width: 200
        },
        {
            name: 'FechaCreacionFormulario',
            index: 'FechaCreacionFormulario',
            hidden: false,
            width: 100
        },
        {
            name: 'ActivoFormulario',
            index: 'ActivoFormulario',
            hidden: false,
            width: 50
        }
    ],
    rowNum: 200,
    rowList: [200, 100, 10],
    pager: '#barraFormularios',
    sortname: 'id',
    loadComplete: function (data) {
        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/


    },
    //height: "100%",//ajustar tamaño a la cantidad de filas
    onSelectRow: function (id) {
        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
        //var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');


    }, ondblClickRow: function () {
        var seleccionGridDerivado = $("#tbFormularios").jqGrid('getGridParam', 'selrow');
        var idFormulario = $("#tbFormularios").jqGrid('getCell', seleccionGridDerivado, 'IdFormulario');

        $("#divConstructorHabilidadesCompetencias").show();
        $("#spNombreFormulario").html(" " + $("#tbFormularios").jqGrid('getCell', seleccionGridDerivado, 'NombreFormulario') + ". ")
        recargarGridHabilidades(idFormulario);

    },
    autowidth: true,
    viewrecords: true,
    sortorder: "desc",
    caption: "Listado de los formularios almacenados"
});
jQuery("#tbFormularios").jqGrid('navGrid', '#barraFormularios', {
    edit: false,
    add: false,
    del: false
});


$("#cmdCrearFormulario").click(function (e) {
    e.preventDefault();

    $("#divDatosFormulario").show();
});
$("#cmdActivoFormulario").click(function (e) {
    e.preventDefault();

    var seleccionGridDerivado = $("#tbFormularios").jqGrid('getGridParam', 'selrow');
    var idFormulario = $("#tbFormularios").jqGrid('getCell', seleccionGridDerivado, 'IdFormulario');

    $.ajax({
        type: "POST",
        url: "/SERH/PGR/Constructor?opcion=activarFormulario&idFormulario=" + idFormulario,
        dataType: 'json',
        success: function (data) {
            if (data.accion == "1") {
                mostrarMensajeFlotante(data.mensaje);
            } else {
                mostrarMensajeFlotante(data.mensaje);
            }


        },
        error: function () {

        }
    });

    recargarGrid();


});
$("#cmdInactivoFormulario").click(function (e) {
    e.preventDefault();
    var seleccionGridDerivado = $("#tbFormularios").jqGrid('getGridParam', 'selrow');
    var idFormulario = $("#tbFormularios").jqGrid('getCell', seleccionGridDerivado, 'IdFormulario');
    $.ajax({
        type: "POST",
        url: "/SERH/PGR/Constructor?opcion=desactivarFormulario&idFormulario=" + idFormulario,
        dataType: 'json',
        success: function (data) {
            if (data.accion == "1") {
                mostrarMensajeFlotante(data.mensaje);
            } else {
                mostrarMensajeFlotante(data.mensaje);
            }


        },
        error: function () {

        }
    });
    recargarGrid();
});


$("#cmdGuardarFormulario").click(function (e) {
    e.preventDefault();
    $("#divDatosFormulario :input").serialize();
    $.ajax({
        type: "POST",
        url: "/SERH/PGR/Constructor?opcion=nuevoFormulario&" + $("#divDatosFormulario :input").serialize(),
        dataType: 'json',
        success: function (data) {
            if (data.accion == "1") {
                alert("formulario creado exitosamente");

            } else {
                mostrarMensajeFlotante(data.mensaje);
            }
            recargarGrid();

        },
        error: function () {
            alert("hubo un error.");
        }
    });
    

});



$("#tbHabilidadesCompentencias").jqGrid({
    //url: '/SERH/PGR/Constructor?opcion=listadoHabilidadesCompentencias',
    datatype: "json",
    colNames: ['IdEvaluacion', 'Codigo', 'IdHabilidad', 'Habilidad/Competencia'],
    colModel: [
        {
            name: 'IdEvaluacion',
            index: 'IdEvaluacion',
            hidden: true,
            align: "left",
            width: 40
        }, {
            name: 'CodigoFormulario',
            index: 'CodigoFormulario',
            hidden: false,
            align: "center",
            width: 55
        },
        {
            name: 'IdHabilidad',
            index: 'IdHabilidad',
            hidden: true,
            width: 50
        },
        {
            name: 'NombreHabilidad',
            index: 'NombreHabilidad',
            hidden: false,
            width: 400
        }
    ],
    rowNum: 200,
    rowList: [200, 100, 10],
    pager: '#barraHabilidadesCompentencias',
    sortname: 'id',
    loadComplete: function (data) {
        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/


    },
    //height: "100%",//ajustar tamaño a la cantidad de filas
    onSelectRow: function (id) {
        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
        //var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');


    },
    ondblClickRow: function () {

    },
    autowidth: true,
    viewrecords: true,
    sortorder: "desc",
    caption: "Listado de habilidades y competencias para el formulario seleccionado"
});
jQuery("#tbHabilidadesCompentencias").jqGrid('navGrid', '#barraHabilidadesCompentencias', {
    edit: false,
    add: false,
    del: false
});


$("#tbComportamientoLaboral").jqGrid({
    //url: '/SERH/PGR/Constructor?opcion=listadoHabilidadesCompentencias',
    datatype: "json",
    colNames: ['IdEvaluacion', 'Codigo', 'IdComportamiento', 'Comportamiento Laboral'],
    colModel: [
        {
            name: 'IdEvaluacion',
            index: 'IdEvaluacion',
            hidden: true,
            align: "left",
            width: 40
        }, {
            name: 'CodigoFormulario',
            index: 'CodigoFormulario',
            hidden: false,
            align: "center",
            width: 55
        },
        {
            name: 'IdComportamiento',
            index: 'IdComportamiento',
            hidden: true,
            width: 50
        },
        {
            name: 'NombreComportamiento',
            index: 'NombreComportamiento',
            hidden: false,
            width: 400
        }
    ],
    rowNum: 200,
    rowList: [200, 100, 10],
    pager: '#barraComportamientoLaboral',
    sortname: 'id',
    loadComplete: function (data) {
        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/


    },
    //height: "100%",//ajustar tamaño a la cantidad de filas
    onSelectRow: function (id) {
        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
        //var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');


    },
    ondblClickRow: function () {

    },
    autowidth: true,
    viewrecords: true,
    sortorder: "desc",
    caption: "Listado del comportamiento laboral para el formulario seleccionado"
});
jQuery("#tbComportamientoLaboral").jqGrid('navGrid', '#barraComportamientoLaboral', {
    edit: false,
    add: false,
    del: false
});
//------------ LISTADOS COMPLETOS ------------------
$("#tbHabilidadesCompentenciasCompleta").jqGrid({
    url: '/SERH/PGR/Constructor?opcion=listadoCompletoHabilidadesCompentencias',
    datatype: "json",
    colNames: ['N.', 'Habilidad/Competencia', 'Fecha Creacion'],
    colModel: [
        {
            name: 'IDHABILIDAD',
            index: 'IDHABILIDAD',
            hidden: false,
            align: "left",
            width: 15
        }, {
            name: 'NOMBREHABILIDAD',
            index: 'NOMBREHABILIDAD',
            hidden: false,
            align: "left",
            width: 350
        },
        {
            name: 'FECHACREACIONHABILIDAD',
            index: 'FECHACREACIONHABILIDAD',
            hidden: true,
            width: 50
        }
    ],
    rowNum: 200,
    rowList: [200, 100, 10],
    pager: '#barraHabilidadesCompentenciasCompleta',
    sortname: 'id',
    loadComplete: function (data) {
        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/


    },
    //height: "100%",//ajustar tamaño a la cantidad de filas
    onSelectRow: function (id) {
        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
        //var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');


    },
    ondblClickRow: function () {

    },
    autowidth: true,
    viewrecords: true,
    sortorder: "desc",
    caption: "Listado de TODAS habilidades y competencias"
});
jQuery("#tbHabilidadesCompentenciasCompleta").jqGrid('navGrid', '#barraHabilidadesCompentenciasCompleta', {
    edit: false,
    add: false,
    del: false
});
$("#tbComportamientoLaboralCompleta").jqGrid({
    url: '/SERH/PGR/Constructor?opcion=listadoCompletoComportamiento',
    datatype: "json",
    colNames: ['N.', 'Comportamiento Laboral', 'Fecha Creacion'],
    colModel: [
        {
            name: 'IDCOMPORTAMIENTO',
            index: 'IDCOMPORTAMIENTO',
            hidden: false,
            align: "left",
            width: 15
        },
        {
            name: 'NOMBRECOMPORTAMIENTO',
            index: 'NOMBRECOMPORTAMIENTO',
            align: "left",
            hidden: false,
            width: 350
        },
        {
            name: 'FECHACREACIONCOMPORTAMIENTO',
            index: 'FECHACREACIONCOMPORTAMIENTO',
            hidden: true,
            width: 50
        }
    ],
    rowNum: 200,
    rowList: [200, 100, 10],
    pager: '#barraComportamientoLaboralCompleta',
    sortname: 'id',
    loadComplete: function (data) {
        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/


    },
    //height: "100%",//ajustar tamaño a la cantidad de filas
    onSelectRow: function (id) {
        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
        //var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');


    },
    ondblClickRow: function () {

    },
    autowidth: true,
    viewrecords: true,
    sortorder: "desc",
    caption: "Listado del comportamiento laboral para el formulario seleccionado"
});
jQuery("#tbComportamientoLaboralCompleta").jqGrid('navGrid', '#barraComportamientoLaboralCompleta', {
    edit: false,
    add: false,
    del: false
});



$("#cmdNuevaHabilidad").click(function (e) {
    e.preventDefault();
    $("#tdNuevaHabilidad").show();
    $("#txtNuevaHabilidad").val("");
});
$("#cmdCancelarNuevaHabilidad").click(function (e) {
    e.preventDefault();
    $("#tdNuevaHabilidad").hide();
    $("#txtNuevaHabilidad").val("");
});
$("#cmdCancelarNuevoComportamiento").click(function (e) {
    e.preventDefault();
    $("#tdNuevoComportamiento").hide();
    $("#txtNuevoComportamiento").val("");
});
$("#cmdNuevoComportamiento").click(function (e) {
    e.preventDefault();
    $("#tdNuevoComportamiento").show();
    $("#txtNuevoComportamiento").val("");
});
/* ALMACENAMIENTO */

$("#cmdAgregarHabilidad").click(function (e) {
    e.preventDefault();
    agregarElmentoFormulario("tbHabilidadesCompentenciasCompleta", "IDHABILIDAD", "tbHabilidadesCompentencias");

});

$("#cmdAgregarComportamiento").click(function (e) {
    e.preventDefault();
    agregarElmentoFormulario("tbComportamientoLaboralCompleta", "IDCOMPORTAMIENTO", "tbComportamientoLaboral");

});

function recargarGrid() {
    jQuery("#tbFormularios").jqGrid().setGridParam({
        url: '/SERH/PGR/Constructor?opcion=listaFormularios',
    }).trigger("reloadGrid");

}

function recargarGridHabilidades(idFormulario) {
    jQuery("#tbHabilidadesCompentencias").jqGrid().setGridParam({
        url: '/SERH/PGR/Constructor?opcion=listadoHabilidadesCompentencias&idFormulario=' + idFormulario,
    }).trigger("reloadGrid");
    jQuery("#tbComportamientoLaboral").jqGrid().setGridParam({
        url: '/SERH/PGR/Constructor?opcion=listadoComportamiento&idFormulario=' + idFormulario,
    }).trigger("reloadGrid");

}

function agregarElmentoFormulario(grid, elemento, gridRecargar) {
    var seleccionGridDerivado = $("#tbFormularios").jqGrid('getGridParam', 'selrow');
    var idFormulario = $("#tbFormularios").jqGrid('getCell', seleccionGridDerivado, 'IdFormulario');

    var seleccionGridElmento = $("#" + grid).jqGrid('getGridParam', 'selrow');
    var idElmento = $("#" + grid).jqGrid('getCell', seleccionGridElmento, elemento);
    //alert(" grid es: " + idFormulario + " idelemento: " + idElmento);
    $.ajax({
        type: "POST",
        url: "/SERH/PGR/Constructor?opcion=" + grid + "&idFormulario=" + idFormulario + "&idElemento=" + idElmento,
        dataType: 'json',
        success: function (data) {
            if (data.accion == "1") {
                mostrarMensajeFlotante(data.mensaje);

            } else {
                mostrarMensajeFlotante(data.mensaje);
            }
            jQuery("#" + gridRecargar).jqGrid().setGridParam({
                datatype: "json",
            }).trigger("reloadGrid");


        },
        error: function () {
            alert("no es posible acceder al sistema informatico presione F5");
        }
    });



}


$("#ddd").click(function(e){
    e.preventDefault();
    
    
    $.ajax({
        type: "POST",
        url: "/SERH/PGR/Constructor?opcion=nuevoComportamiento&"+$("#tdNuevoComportamiento :input").serialize(),
        dataType: 'json',
        success: function (data) {
            if (data.accion == "1") {
                mostrarMensajeFlotante(data.mensaje);

            } else {
                mostrarMensajeFlotante(data.mensaje);
            }
            


        },
        error: function () {
            alert("no es posible acceder al sistema informatico presione F5");
        }
    });
    
});