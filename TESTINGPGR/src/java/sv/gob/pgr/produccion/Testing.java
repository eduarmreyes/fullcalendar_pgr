/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.pgr.produccion;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WNRO
 */
@WebServlet(name = "Testing", urlPatterns = {"/Testing"})
public class Testing extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

//        SecurityContext secContex = SecurityContextHolder.getContext();
//        SerhUser user = (SerhUser) secContex.getAuthentication().getPrincipal();
//        //---------------- FIN DE VARIABLES DEL CONTEXTO----------------------//
//
//        HUM_ResultadoEvaluacion hum_resultadoEvaluacion = new HUM_ResultadoEvaluacion();
//        HUM_Evaluacion hum_evaluacion = new HUM_Evaluacion();
//        HUM_Empleados hum_empleados = new HUM_Empleados();
//        GLO_UnidadesOrganizacionales glo_UnidadesOrganizacionales = new GLO_UnidadesOrganizacionales();
//        HUM_ResultadoClima hum_ResultadoClima = new HUM_ResultadoClima();
//        HUM_Puestos hum_Puestos = new HUM_Puestos();

        String formaJson = "";
        try {
            String opcion = request.getParameter("opcion");

            //aca recibimos a la variable
            String formulario = request.getParameter("formulario");

            if (opcion.equals("listadoPuestos")) {

                 formaJson = "{\"accion\":\"1\"}";

            } else {
                System.out.println("basura");
            }

            System.out.println(formaJson);
            response.getWriter().write(formaJson);

        } finally {
            //out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
