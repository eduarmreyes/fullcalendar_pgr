<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>

   <link rel="stylesheet" type="text/css" href="../../static/css/webstyle.css">
   <link rel="stylesheet" type="text/css" href="../../static/css/jquery-ui-1.8.23.custom.css">
   <link rel="stylesheet" type="text/css" href="../../static/css/ui.jqgrid.css">
   <link rel="stylesheet" type="text/css" href="../../static/css/menu_style.css">




<link href='../../static/js/config/fullcalendar.css' rel='stylesheet' />
<link href='../../static/js/config/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../../static/js/config/lib/moment.min.js'></script>
<script src='../../static/js/config/lib/jquery.min.js'></script>
<script src='../../static/js/config/fullcalendar.min.js'></script>
<script src='../../static/js/config/locale/es.js'></script>

<style>
    .btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
 
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
    
    

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>    <title>Calendar</title>
</head>
<body>

   
        
        
        
           <div class="wrapper">
            <div class="shadowcontainer">
                <div>
                    <div class="maincontainer" >
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Top.jspf"/>--%>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Menu.jspf"/>--%>
                        </div>
                        <div>
                            <div class="workareacontainer ui-widget-content">
                                <div class="container" >
                                    <div style="text-align: center" ><h1>MARCACIONES</h1></div>
                                </div>
                                <div class="container">


                                
                                    <div style="margin: 0 auto; padding-left: 10px; "> <input type="button" id="nextbutton" value="next" class="btn"> 
    <input type="button" id="prevbutton" value="prev" class="btn"></div>
	<div id='calendar'>   </div>
                                    
                                    
                                    
                                </div>



                         


                              
                            </div>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Footer.jspf"/>--%>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        
        
        
        
        

</body>

<script src="../../static/js/general/calendar.js" type="text/javascript"></script>
</html>
