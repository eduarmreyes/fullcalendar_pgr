/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$("#tabsBusqueda").tabs();

$(".buscador").button({
    icons: {
        primary: "ui-icon ui-icon-search"
    },
    text: false
});
$(".cancelador").button({
    icons: {
        primary: "ui-icon ui-icon-closethick"
    },
    text: false
});
$("#cmdCargarFormulario,#cmdLimpiarTodo").button();
$("#cmdGuardarFormulario").button({
    icons: {
        primary: "ui-icon ui-icon-disk"
    }
});
$("#cmdLimpiarFormulario").button({
    icons: {
        primary: "ui-icon ui-icon-refresh "
    }
});
$("#cmdCancelarFormulario,#cmdCancelarEvaluador").button({
    icons: {
        primary: "ui-icon ui-icon-closethick"
    }
});

$("#cmdEditarEmpleado").button({
    icons: {
        primary: "i-icon ui-icon-pencil"
    }
});

$("#cmdGuardarNombre,#cmdGuardarApellido,#cmdGuardarCargo,#cmdGuardarProcuraduria,#cmdGuardarUnidad").button({
    icons: {
        primary: "ui-icon ui-icon-disk"
    }
});
$("#cmdCancelarEdicion").button({
    icons: {
        primary: "ui-icon ui-icon-arrowthickstop-1-n"
    }
});



$('#txtFechaEvaluado').mask("99/99/2099");
$('#txtFechaEvaluado').focusout(function (e) {
    if (validarFecha($(this).val())) {
        $(this).removeClass("warning");
    } else {
        $(this).addClass("warning").val("");
    }
});
$("#txtFechaEvaluado").click(function () {
    $(this).removeClass("warning");
});

$("#cmdCancelarEvaluador").click(function (e) {
    e.preventDefault();
    $("#txtNombreEvaluador").prop("readonly", false).val("");
    $("#txtCodigoEvaluador").val("");
});


var vector = new Array();
$.ajax({
    type: "POST",
    url: "/SERH/PGR/IngresoFormulario?opcion=EmpleadosPgr",
    dataType: 'json',
    success: function (data) {
        var dataJson = data;
        $.each(data.rows, function (i, rows) {
            vector[i] = rows.cell[3] + ' ' + rows.cell[4];
        });
        /* verificar la cantidad de datos*/
        if (data.rows.length == 1) {
            alert("ha ocurrido un error inesperado. Por favor contactese con el Administrador del Sistema Informatico. Codigo ERROR: S8");
        } else {
            $("#txtBusquedaEmpleado").autocomplete({
                source: vector,
                minLength: 3,
                select: function (event, ui) {
                    seleccion = ui.item.value;
                    /*comparar para determinar el codigo de la pretension el cual sera almaceando*/
                    $.each(dataJson.rows, function (j, rows) {
                        if (seleccion == rows.cell[3] + ' ' + rows.cell[4]) {
                            /*Esta busqueda permite la seleccion del codigo de la pretension */
                            $("#txtCodigoEmp").val(rows.cell[0]);
                            $("#txtNombreEmpleado").val(rows.cell[3]);
                            $("#txtApellidoEmpleado").val(rows.cell[4]);
                            $("#txtCargoEmpleado").val(rows.cell[6]);
                            $("#txtUnidadEmpleado").val(rows.cell[8]);
                            $("#txtProcuraduriaEmpleado").val(rows.cell[12]);
                            //$("#txtPretensionSeleccionada").attr("value", codigoPretension);
                        }
                    });

                }
            });
            seleccionarEvaluador(vector, dataJson);
        }
    },
    error: function (data) {
        /* Mensajes de alerta */
        alertarMensaje();
        /*colocar un identificador en la pantalla para decir que existe un error*/
    }

});

cagarAutocompleteBasico('IngresoFormulario', 'ListaPuestos','txtNombreCargo','txtCodigoCargo');
cagarAutocompleteBasico('IngresoFormulario', 'ListaUnidades','txtNombreUnidad','txtCodigoUnidad');

function seleccionarEvaluador(vector, dataJson) {
    $("#txtNombreEvaluador").autocomplete({
        source: vector,
        minLength: 3,
        select: function (event, ui) {
            seleccion = ui.item.value;
            /*comparar para determinar el codigo de la pretension el cual sera almaceando*/
            $.each(dataJson.rows, function (j, rows) {
                if (seleccion == rows.cell[3] + ' ' + rows.cell[4]) {
                    /*Esta busqueda permite la seleccion del codigo de la pretension */
                    $("#txtCodigoEvaluador").val(rows.cell[0]).removeClass("warning");
                    $("#txtNombreEvaluador").prop("readonly", true);
                }
            });

        }
    });
}

$("#divEmpleados,#divCargo,#divUnidad,#divPgr,#divEdicionDatos").hide();

$("#cmdCargarFormulario").click(function (e) {
    e.preventDefault();

    if (validarCampos("busqueda1")) {
        var formulario = $("#slRecursosHumanos").val();
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=habilidades&formulario=" + formulario,
            dataType: 'json',
            success: function (data) {

                var codigoTabla = "<h3>EVALUACION DE LA COMPETENCIA TECNICA</h3><table id='tbHabilidades' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;'>";
                $.each(data.rows, function (i, rows) {
                    // siempre 4 filas en este apartado
                    //codigoTabla += "<tr><td>" + rows.cell[2] + "</td> <td> <input type='text' id='hab-" + rows.cell[1] + "' name='hab-" + rows.cell[1] + "' size='4' maxlength='1' style='text-align:center' onKeyPress='return Restringido(event)' /> </td></tr>";
                    codigoTabla += "<tr><td>" + rows.cell[2] + "</td> <td> <input type='number'  min='1' max='10' id='hab-" + rows.cell[1] + "' name='hab-" + rows.cell[1] + "' size='4' maxlength='1' style='text-align:center' /> </td></tr>";
                });
                codigoTabla += "</table> <p>-</p>";
                $("#divHabilidades").html(codigoTabla);
                //mostrar
            },
            error: function () {

            }
        });
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=comportamiento&formulario=" + formulario,
            dataType: 'json',
            success: function (data) {
                var codigoTabla = "<h3>EVALUACION DEL COMPORTAMIENTO LABORAL</h3><table id='tbComportamiento'  border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;'  >";

                $.each(data.rows, function (i, rows) {
                    // siempre 4 filas en este apartado
                    //codigoTabla += "<tr><td>" + rows.cell[2] + "</td> <td> <input type='text' id='comp-" + rows.cell[1] + "' name='comp-" + rows.cell[1] + "' size='3' maxlength='2' onKeyPress='return Restringido(event)' style='text-align:center' /> </td></tr>";
                    codigoTabla += "<tr><td>" + rows.cell[2] + "</td> <td> <input type='number'  min='1' max='10' id='comp-" + rows.cell[1] + "' name='comp-" + rows.cell[1] + "' size='3' maxlength='2'   style='text-align:center' /> </td></tr>";
                });
                codigoTabla += "</table> <p>-</p>";
                $("#divComportamiento").html(codigoTabla);

            },
            error: function () {

            }
        });
//------------------------------------------------------------------------------
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=matriz&formulario=" + formulario,
            dataType: 'json',
            success: function (data) {
                if (data.data != "") {
                    $("#divMatriz").html("<h1>MATRIZ DE COMPETENCIA TECNICA</h1>" + data.data);//Crea la tabla
                }
                $(".mytable").rotateTableCellContent();//Rotacion y estilo
            },
            error: function () {
                errorServidor();
            }
        });
        // ----------------------------------------------------------------------------
//        restringirNumeracion();
        $("#divOpcionesFormulario,#divObservaciones").show();
    } else {
        alert("Antes de mostrare el formulario de evaluacion es necesario completar los datos de la persona a evaluar. Por favor complete los campos marcado con color rojo");
    }

});

$.ajax({
    type: "POST",
    url: "/SERH/PGR/IngresoFormulario?opcion=ListaFormularios",
    dataType: 'json',
    success: function (data) {
        $.each(data.rows, function (i, rows) {
            codigo = "<option value='" + rows.cell[0] + "'>" + rows.cell[1] + "</option>";
            $("#slRecursosHumanos").append(codigo);

        });
    },
    error: function () {

    }
});


$.ajax({
    type: "POST",
    url: "/SERH/PGR/IngresoFormulario?opcion=ListaPuestos",
    dataType: 'json',
    success: function (data) {
        $("#slNombreCargo").append("<option value='00'>-SELECCIONE-</option>");
        $.each(data.rows, function (i, rows) {
            codigo = "<option value='" + rows.cell[0] + "'>" + rows.cell[1] + "</option>";
            $("#slNombreCargo").append(codigo);

        });
    },
    error: function () {
        errorServidor();
    }
});



$("#cmdGuardarNombre").click(function (e) {
    e.preventDefault();

    if ($("#txtnombreEditar").val() != "") {
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=editarNombres&txtnombreEditar=" + $("#txtnombreEditar").val() + "&txtCodigoEmp=" + $("#txtEditarCodigoEmp").val(),
            dataType: 'json',
            success: function (data) {
                if (data.accion == "1") {
                    alert("Se ha actualizado con exito los nombres de la persona");
                } else {
                    alert("Al parecer hubo un error al actualizar el nombre del empleado/a. Si el problema persiste contacte con el administrador");
                }
            },
            error: function () {
                errorServidor();
            }
        });
    } else {
        alert("El nombre esta vacio. Coloque un nombre.");
    }

});
$("#cmdGuardarApellido").click(function (e) {
    e.preventDefault();
    if ($("#txtnombreEditar").val() != "") {
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=editarApellidos&txtApellidosEditar=" + $("#txtApellidosEditar").val() + "&txtCodigoEmp=" + $("#txtEditarCodigoEmp").val(),
            dataType: 'json',
            success: function (data) {
                if (data.accion == "1") {
                    alert("Se ha actualizado con exito los APELLIDOS de la persona");
                } else {
                    alert("Al parecer hubo un error al actualizar el APELLLIDO del empleado/a. Si el problema persiste contacte con el administrador");
                }
            },
            error: function () {
                errorServidor();
            }
        });
    } else {
        alert("El apellido esta vacio. Coloque un apellido.");
    }

});
$("#cmdGuardarCargo").click(function (e) {
    e.preventDefault();
    if ($("#txtCodigoCargo").val()!="") {
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=editarCargo&codigoCargo=" + $("#txtCodigoCargo").val() + "&txtCodigoEmp=" + $("#txtEditarCodigoEmp").val(),
            dataType: 'json',
            success: function (data) {
                if (data.accion == "1") {
                    alert("Se ha actualizado con exito EL CARGO de la persona");
                } else {
                    alert("Al parecer hubo un error al actualizar el CARGO del empleado/a. Si el problema persiste contacte con el administrador");
                }
            },
            error: function () {
                errorServidor();
            }
        });
    } else {
        alert("El CARGO esta vacio. Coloque un apellido.");
    }
});


$("#cmdGuardarProcuraduria").click(function (e) {
    e.preventDefault();

});

$("#cmdGuardarUnidad").click(function (e) {
    e.preventDefault();
    if ($("#txtCodigoUnidad").val()!="") {
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=editarUnidad&codigoUnidad=" + $("#txtCodigoUnidad").val() + "&txtCodigoEmp=" + $("#txtEditarCodigoEmp").val(),
            dataType: 'json',
            success: function (data) {
                if (data.accion == "1") {
                    alert("Se ha actualizado con exito LA UNIDAD de la persona");
                } else {
                    alert("Al parecer hubo un error al actualizar LA UNIDAD del empleado/a. Si el problema persiste contacte con el administrador");
                }
            },
            error: function () {
                errorServidor();
            }
        });
    } else {
        alert("LA UNIDAD esta vacio. Coloque en la UNIDAD.");
    }

});


function errorServidor() {
    Alert("Hubo un error al ejecutar la consulta, al parecer la sesion se ha finalizado. Cierre el sistema SERH y vuelva a ingresar.");
}


//function restringirNumeracion() {
//    $("#TdMatriz :input").jStepper({minValue: 1, maxValue: 10, minLength: 1});
//    $("#tbComportamiento :input").jStepper({minValue: 1, maxValue: 10, minLength: 1});
//    $("#tbHabilidades :input").jStepper({minValue: 1, maxValue: 10, minLength: 1});
//}

$("#cmdGuardarFormulario").click(function (e) {
    e.preventDefault();
    //Validar Matriz:
    var m = validarDatos("TdMatriz");
    var n = validarDatos("tbComportamiento");
    var o = validarDatos("tbHabilidades");
    var x = validarCampos("tbDatosRegistros");
    var fecha = $("#txtFechaEvaluado").val();
    var evaluador = $("#txtCodigoEvaluador").val();
    if (evaluador == "") {
        $("#txtCodigoEvaluador").addClass("warning");
    }
    if (fecha == "") {
        $("#txtFechaEvaluado").addClass("warning");
    }
    if (m && n && o && x && fecha != "" && evaluador != "") {
        //ejecutar;
        //alert("Se esta iniciando el proceso de registro de la Evaluación del Desempeño");
        var matrixDatos = $("#TdMatriz :input").serialize();
        var comportamientoDatos = $("#tbComportamiento :input").serialize();
        var habilidadesDatos = $("#tbHabilidades :input").serialize();
        var registrosDatos = $("#tbDatosRegistros :input").serialize();
        var observacionesEmpleado = $("#divObservaciones :input").serialize();
        var formulario = $("#slRecursosHumanos").val();
        $("#divCargando").html("Enviando datos! \n Se esta iniciando el proceso de registro de la Evaluación del Desempeño... por favor espere a que desaparesca este mensajes")
        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=registroMasivo&" + matrixDatos + "&" + comportamientoDatos + "&" + habilidadesDatos + "&" + observacionesEmpleado + "&" + registrosDatos + "&formulario=" + formulario,
            dataType: 'json',
            success: function (data) {
                $("#divCargando").html("");
                if (data.tipo == "-1") {
                    $("#divCargando").html("Le recomendamos enfaticamente que elimine esta evaluacion y la vuelva a CREAR NUEVA");
                } else if (data.tipo == "0") {
                    $("#divCargando").html("EXISTEN PROBLEMAS CON EL SERVIDOR");
                } else if (data.tipo == "1") {
                    mostrarMensajeFlotante(data.resultado);
                    $("#divComportamiento").html("");
                    $("#divHabilidades").html("");
                    $("#divMatriz").html("");
                    $("#divObservaciones :input").val("");
                    $("#divOpcionesFormulario,#divObservaciones").hide();
                    $("#txtNombreEvaluador").prop("readonly", false).val("");

                }

            },
            error: function () {
                errorServidor();
            }
        });

    } else {
        alert("Por favor verifique los campos que no se han calificado. Estos campos seran marcados en color rojo");
    }

});

function validarCampos(elemento) {
    var valido = true;
    $("#" + elemento + " :input[type=text]").each(function (i) {
        var campo = $(this).val();
        if (campo == "") {
            $(this).addClass("warning");
            valido = false;
        }

    });
    $("input[type=text]").click(function () {
        $(this).removeClass("warningText");
        $(this).removeClass("warning");
    });
    return valido;

}

function validarDatos(elemento) {
    var valido = true;
    $("#" + elemento + " :input[type=number]").each(function (i) {
        var campo = $(this).val();
        if (campo == "" || campo.length == 0 || campo < 0 || campo > 10) {
            $(this).addClass("warning");
            valido = false;
        }

    });
    $("input[type=number]").focusin(function () {
        $(this).removeClass("warningText");
        $(this).removeClass("warning");
    })
    $("input[type=number]").click(function () {
        $(this).removeClass("warningText");
        $(this).removeClass("warning");
    });
    return valido;

}



/* grid de empleados pgr*/



//$("#tbEmpleadoPgr").jqGrid({
//    url: '/SERH/PGR/IngresoFormulario?opcion=EmpleadosPgr',
//    datatype: "json",
//    colNames: ['Codigo', 'Nombres', 'Apellidos', 'Sexo', 'Revision'],
//    colModel: [
//        {
//            name: 'CodigoPersona',
//            index: 'CodigoPersona',
//            hidden: false,
//            align: "center",
//            width: 55
//        },
//        {
//            name: 'NombresPersonaNatural',
//            index: 'NombresPersonaNatural',
//            hidden: false,
//            width: 55
//        },
//        {
//            name: 'ApellidosPersonaNatural',
//            index: 'ApellidosPersonaNatural',
//            hidden: false,
//            width: 55
//        },
//        {
//            name: 'SexoPersonaNatural',
//            index: 'SexoPersonaNatural',
//            hidden: false,
//            align: "center",
//            width: 70
//        },
//        {
//            name: 'Modificacion',
//            index: 'Modificacion',
//            align: "center",
//            width: 70
//        }
//    ],
//    rowNum: 2000,
//    rowList: [2000, 1000, 500],
//    pager: '#barraEmpleadoPgr',
//    sortname: 'id',
//    loadComplete: function(data) {
//        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/
//
//
//    },
//    //height: "100%",//ajustar tamaño a la cantidad de filas
//    onSelectRow: function(id) {
//        // escribir empleado:
//        var seleccionGridDerivado = $("#tbEmpleadoPgr").jqGrid('getGridParam', 'selrow');
//        $("#txtCodigoEmp").val($("#tbEmpleadoPgr").jqGrid('getCell', seleccionGridDerivado, 'CodigoPersona'));
//        $("#txtNombreEmpleado").val($("#tbEmpleadoPgr").jqGrid('getCell', seleccionGridDerivado, 'NombresPersonaNatural'));
//        $("#txtApellidoEmpleado").val($("#tbEmpleadoPgr").jqGrid('getCell', seleccionGridDerivado, 'ApellidosPersonaNatural'));
//        borrarAdvertensia();
//
//
//    },
//    autowidth: true,
//    viewrecords: true,
//    sortorder: "desc",
//    caption: "Empleados de la PGR"
//});
//jQuery("#tbEmpleadoPgr").jqGrid('navGrid', '#barraEmpleadoPgr', {
//    edit: false,
//    add: false,
//    del: false
//});
//
////
//

///*- procuradurias -*/
//$("#tbPgr").jqGrid({
//    url: '/SERH/PGR/IngresoFormulario?opcion=NombresProcuradurias',
//    datatype: "json",
//    colNames: ['Codigo', 'Nombre de Procuraduria'],
//    colModel: [
//        {
//            name: 'CodigoUnidadOrganizacional',
//            index: 'CodigoUnidadOrganizacional',
//            hidden: false,
//            align: "center",
//            width: 55
//        },
//        {
//            name: 'NombreUnidadOrganizacional',
//            index: 'NombreUnidadOrganizacional',
//            hidden: false,
//            width: 250
//        }
//    ],
//    rowNum: 50,
//    rowList: [50, 100, 2000],
//    pager: '#barraPgr',
//    sortname: 'id',
//    loadComplete: function(data) {
//        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/
//
//
//    },
//    //height: "100%",//ajustar tamaño a la cantidad de filas
//    onSelectRow: function(id) {
//        var seleccionPgr = $("#tbPgr").jqGrid('getGridParam', 'selrow');
//        $("#txtProcuraduriaEmpleado").val($("#tbPgr").jqGrid('getCell', seleccionPgr, 'NombreUnidadOrganizacional'));
//        borrarAdvertensia();
//    },
//    autowidth: true,
//    viewrecords: true,
//    sortorder: "desc",
//    caption: "Listado de Procuradurías"
//});
//jQuery("#tbPgr").jqGrid('navGrid', '#barraPgr', {
//    edit: false,
//    add: false,
//    del: false
//});
//
////******** UNIDADES
//$("#tbUnidad").jqGrid({
//    url: '/SERH/PGR/IngresoFormulario?opcion=NombresUnidades',
//    datatype: "json",
//    colNames: ['Codigo', 'Nombre de la Unidad', 'Tipo Unidad'],
//    colModel: [
//        {
//            name: 'CodigoUnidadOrganizacional',
//            index: 'CodigoUnidadOrganizacional',
//            hidden: false,
//            align: "center",
//            width: 55
//        },
//        {
//            name: 'NombreUnidadOrganizacional',
//            index: 'NombreUnidadOrganizacional',
//            hidden: false,
//            width: 250
//        },
//        {
//            name: 'NombreTipoUnidadOrganizacional',
//            index: 'NombreTipoUnidadOrganizacional',
//            hidden: false,
//            width: 250
//        }
//    ],
//    rowNum: 100,
//    rowList: [100, 2000],
//    pager: '#barraUnidad',
//    sortname: 'id',
//    loadComplete: function(data) {
//        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/
//
//
//    },
//    //height: "100%",//ajustar tamaño a la cantidad de filas
//    onSelectRow: function(id) {
//        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
//        var seleccionPgr = $("#tbUnidad").jqGrid('getGridParam', 'selrow');
//        $("#txtUnidadEmpleado").val($("#tbUnidad").jqGrid('getCell', seleccionPgr, 'NombreUnidadOrganizacional'));
//        borrarAdvertensia();
//    },
//    autowidth: true,
//    viewrecords: true,
//    sortorder: "desc",
//    caption: "Listado de Unidades Activas en las diferentes Procuradurias"
//});
//jQuery("#tbUnidad").jqGrid('navGrid', '#barraUnidad', {
//    edit: false,
//    add: false,
//    del: false
//});
//
///* CARGOS DE EMPLEADO */
//
//$("#tbCargo").jqGrid({
//    url: '/SERH/PGR/IngresoFormulario?opcion=CargosEmpleado',
//    datatype: "json",
//    colNames: ['Codigo', 'Nombre del Cargo', 'Descripción'],
//    colModel: [
//        {
//            name: 'CodigoPuesto',
//            index: 'CodigoPuesto',
//            hidden: false,
//            align: "center",
//            width: 55
//        },
//        {
//            name: 'NombrePuesto',
//            index: 'NombrePuesto',
//            hidden: false,
//            width: 250
//        },
//        {
//            name: 'DescripcionPuesto',
//            index: 'DescripcionPuesto',
//            hidden: false,
//            width: 250
//        }
//    ],
//    rowNum: 200,
//    rowList: [200, 100, 10],
//    pager: '#barraCargo',
//    sortname: 'id',
//    loadComplete: function(data) {
//        /*este dato servirar para mostrar el número de caso a enviar para el procedimiento almacenado*/
//
//
//    },
//    //height: "100%",//ajustar tamaño a la cantidad de filas
//    onSelectRow: function(id) {
//        //var seleccion = $("#tbInvolucrados").jqGrid('getCell',id,'CodigoCaso');
//        var seleccionPgr = $("#tbCargo").jqGrid('getGridParam', 'selrow');
//        $("#txtCargoEmpleado").val($("#tbCargo").jqGrid('getCell', seleccionPgr, 'NombrePuesto'));
//        borrarAdvertensia();
//
//    },
//    autowidth: true,
//    viewrecords: true,
//    sortorder: "desc",
//    caption: "Listado de los diferentes Cargos a ejercer"
//});
//jQuery("#tbCargo").jqGrid('navGrid', '#barraUnidad', {
//    edit: false,
//    add: false,
//    del: false
//});


$("#cmdBuscarPersona,#cmdBuscarCargo,#cmdBuscarUnidad,#cmdCargarFormulario,#cmdBuscarProcuraduria").click(function (e) {
    e.preventDefault();
});

$("#cmdCancelarUnidad").click(function (e) {
    e.preventDefault();
    $("#txtUnidadEmpleado").val("");
    borrarAdvertensia();
});
$("#cmdCancelarProcuraduria").click(function (e) {
    e.preventDefault();
    $("#txtProcuraduriaEmpleado").val("");
    borrarAdvertensia();
});
$("#cmdLimpiarTodo").click(function (e) {
//    e.preventDefault();
//    $("#txtNombreEmpleado,#txtCodigoEmp,#txtApellidoEmpleado,#txtCargoEmpleado,#txtUnidadEmpleado,#txtProcuraduriaEmpleado").val("");
//    borrarAdvertensia();
});

$("#cmdEditarEmpleado").click(function (e) {
    e.preventDefault();
    if ($("#txtCodigoEmp").val() != "") {
        $("#divEdicionDatos").show(500);
        $("#txtEditarCodigoEmp").val($("#txtCodigoEmp").val());
        $("#divEncabezadoFormulario").hide(500);

        $("#txtnombreEditar").val($("#txtNombreEmpleado").val());
        $("#txtApellidosEditar").val($("#txtApellidoEmpleado").val());



    } else {
        alert("Es necesario seleccionar a un empleado");
    }

});


function borrarAdvertensia() {
    $("#tbDatosRegistros  :input[type=text]").each(function (i) {
        $(this).removeClass("warning");
    });
}

$("#cmdLimpiarFormulario").click(function (e) {
    e.preventDefault();
    $("#txtObservaciones").val("");
    $("#txtNombreEvaluador").val("");
    $("#TdMatriz  :input[type=text]").each(function (i) {
        $(this).removeClass("warning");
        $(this).val("0");
    });
    $("#tbComportamiento  :input[type=text]").each(function (i) {
        $(this).removeClass("warning");
        $(this).val("0");
    });
    $("#tbHabilidades  :input[type=text]").each(function (i) {
        $(this).removeClass("warning");
        $(this).val("0");
    });
    $("#tbDatosRegistros  :input[type=text]").each(function (i) {
        $(this).removeClass("warning");
    });
});

$("#cerrarNoticia").click(function () {
    removerMensajeAdvertencia();
    removerMensajeError();
});







function mostrarMensajeFlotante(mensaje) {
    $("#divNoticia").addClass("ui-state-highlight ui-corner-all");
    $("#iconoNoticia").addClass("ui-icon ui-icon-info");
    $("#cerrarNoticia").addClass("ui-icon ui-icon-circle-close");
    $("#textoNoticia").html(mensaje);
    $("#divInformativo").show().fadeIn(10000, function () {
//        $("#divInformativo").fadeOut(9000);
//        //        removerMensajeOK();
//        $("#divNoticia").removeClass("ui-state-highlight ui-corner-all");
//        $("#iconoNoticia").removeClass("ui-icon ui-icon-info ui-icon-alert");
//        $("#cerrarNoticia").removeClass("ui-icon ui-icon-circle-close");
//        $("#textoNoticia").html(mensaje);
    });
}

function removerMensajeAdvertencia() {
    $("#divNoticia").removeClass("ui-state-highlight ui-corner-all");
    $("#iconoNoticia").removeClass("ui-icon ui-icon-info ui-icon-alert");
    $("#cerrarNoticia").removeClass("ui-icon ui-icon-circle-close");
    $("#textoNoticia").html("")
    $("#divInformativo").hide();
}


